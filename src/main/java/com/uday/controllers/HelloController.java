/**
 * 
 */
package com.uday.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Ravikiran J
 *
 */
@Controller
public class HelloController {

	private static final Logger logger = LoggerFactory.getLogger(HelloController.class);

	@RequestMapping("/hi")
	public String showForm() {
		logger.info("welcome to index page");
		return "welcome";
	}



}
