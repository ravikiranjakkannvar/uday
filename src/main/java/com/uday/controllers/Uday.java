/**
 * 
 */
package com.uday.controllers;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author Ravikiran J
 *
 */
@Entity
public class Uday {


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String company;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}







}
