/**
 * 
 */
package com.uday.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Ravikiran J
 *
 */
@Controller
public class CandidateController {

	Logger logger=LoggerFactory.getLogger(CandidateController.class);

	@Autowired 
	CandidateServ serv;

	@RequestMapping("/candidate")
	public String showCandidate() {
		return "candidate";
	}

	@PostMapping("/addcandiate")
	public String addStudent(@ModelAttribute("candidate")Candidate candidate,Model model) {
		serv.save(candidate);
		logger.info("save succesfully");
		return "redirect:/candidate?status=true";
	}


}
