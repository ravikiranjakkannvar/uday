/**
 * 
 */
package com.uday.controllers;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Ravikiran J
 *
 */
@Service

public class CandidateServ  {

	@Autowired
	private CandidateRepo repo;

	public Candidate save(Candidate candidate) {
		return repo.save(candidate);
	}


}
