/**
 * 
 */
package com.uday.controllers;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @author Ravikiran J
 *
 */
@Repository
public interface CandidateRepo extends JpaRepository<Candidate, Integer>{

}
