package com.uday.simpletest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;



@ComponentScan({"com.uday"}) // controller scans for package what we provide
@SpringBootApplication
@EntityScan("com.uday")
@EnableJpaRepositories({"com.uday"})
@EnableAutoConfiguration
public class SimpletestApplication  extends SpringBootServletInitializer {

	/**
	 * Allows you to configure your application 
	 * when it’s launched by the servlet container
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(SimpletestApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(SimpletestApplication.class, args);
	}
}
